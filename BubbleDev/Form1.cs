﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BubbleDev
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.Parent = panel1;
            pictureBox1.BackColor = Color.Transparent;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {


        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {

            int loadingBar = progressBar1.Value;

            if (loadingBar <= 100)
            {
                progressBar1.Value = loadingBar + 6;
            }

            else
            {
                TelaLogin telaLogin = new TelaLogin();
                telaLogin.Show();
                timer1.Enabled = false;
                this.Visible = false;
            }
        }
    }
}

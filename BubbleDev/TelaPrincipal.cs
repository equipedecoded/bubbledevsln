﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BubbleDev
{
    public partial class TelaPrincipal : Form
    {
        public TelaPrincipal()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void TelaPrincipal_Load(object sender, EventArgs e)
        {
            //Configurando fundo personalizado do texto
            label1.Parent = pictureBox1;
            label1.BackColor = Color.Transparent;

            label2.Parent = pictureBox1;
            label2.BackColor = Color.Transparent;

            label3.Parent = pictureBox1;
            label3.BackColor = Color.Transparent;

            label4.Parent = pictureBox1;
            label4.BackColor = Color.Transparent;

            label5.Parent = pictureBox1;
            label5.BackColor = Color.Transparent;

            label6.Parent = pictureBox1;
            label6.BackColor = Color.Transparent;

            //Configurando o PNG
            pictureBox2.Parent = pictureBox1;
            pictureBox2.BackColor = Color.Transparent;

            pictureBox3.Parent = pictureBox1;
            pictureBox3.BackColor = Color.Transparent;

            pictureBox4.Parent = pictureBox1;
            pictureBox4.BackColor = Color.Transparent;

            pictureBox5.Parent = pictureBox1;
            pictureBox5.BackColor = Color.Transparent;

            //Exibindo data do sistema no programa
            string date = DateTime.Now.ToShortDateString();
            label1.Text = date;

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            TelaAdicionar telaAdicionar = new TelaAdicionar();
            telaAdicionar.Show();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            TelaConsultar telaConsultar = new TelaConsultar();
            telaConsultar.Show();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            TelaUtilitario telaUtilitario = new TelaUtilitario();
            telaUtilitario.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
